// 1.5

// Un text fix, text editable i un botó, amb Linearlayout





package edu.upc.cs.damo;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;
import android.widget.LinearLayout;


public class activitat extends Activity {
    /** Called when the activity is first created. */
	
	static String SALUTACIO = "Hola a tothom";
	static String TEXTBOTO = "Un botó";

	TextView text;
	EditText camp;
	Button boto;
	LinearLayout layout;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // Construim els elements a visualitzar
        
        text = new TextView(this);
    	text.setText(SALUTACIO);
    	
    	camp = new EditText(this);
    	
    	boto = new Button(this);
    	boto.setText(TEXTBOTO);
    	
    	// Introduim els elements en un contenidor
    	layout = new LinearLayout(this);
     	layout.addView(text);
    	layout.addView(camp);
    	layout.addView(boto);
    	     	
     	// Canviem l'orientació
     	layout.setOrientation(LinearLayout.VERTICAL);
     	
     	// Demanem al camp que ocupi més espai
        camp.setWidth(200);
      
        setContentView(layout);
    }
}
